<div align="center">
<div>
<!---
Check branch name and files name.
-->
<a href="../-/jobs/artifacts/main/raw/out/FERRON_relazione-readable.pdf?job=compile"><img src="https://img.shields.io/badge/download-pdf-green?style=for-the-badge" alt="readable"></a>
<a href="../-/jobs/artifacts/main/raw/out/figures.zip?job=compile"><img src="https://img.shields.io/badge/download-figures-green?style=for-the-badge" alt="figures"></a>
</div>
</div>

# Computational Statistics

Esercizi per l'esame di Computational Statistics del prof. Enrico Ferrero per l'A.A. 2021/22.

## Consegne

Di seguito le consegne degli esercizi, il cui codice, per intero, non è stato pubblicato.

### Esercizio 1

CALCOLO DEI MOMENTI e verifica relazioni tra centrati e non centrati

### Esercizio 2

Calcolo del giorno tipo per la temperatura: media su un mese per ogni minuto del giorno.

### Esercizio 3

Verifica grafica della PDF delle fluttuazioni di velocità per diversi periodi temporali.

### Esercizio 4

Calcolo della PDF delle fluttuazioni di velocità, verifica gaussianità tramite test chi-quadrato su i seguenti intervalli temporali giorno, mese, e suddivisi giorno-notte

### Esercizio 5

Calcolo delle tempo Lagrangiano tramite regressione esponenziale della funzione di autocorrelazione temporale

### Esercizio 6

Realizzazione del Modello di Langevin per il caso unidimensionale omogeneo e PDF gaussiana. 
Si disegnino le traiettorie in un piano $(t,x)$. Si calcolino le statistiche delle velocità ottenute e si confrontino con il valor medio (nullo) e la deviazione standard usata in input.
