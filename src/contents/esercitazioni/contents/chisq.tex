% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\section{Test \(\chi^{2}\) e \gof}
\label{sec:chisq}

Nel precedente \namecref{sec:fluctuations} abbiamo presentato uno script che plotta la \pdf delle fluttuazioni di velocità per diversi periodi temporali.
Ora, grafici alla mano, potremmo chiederci: quale delle due distribuzioni in \Cref{fig:examples-fluctuations} è gaussiana?
Per rispondere a questa domanda possiamo innanzitutto osservare che nessuna delle due \pdf ha una \emph{perfetta} forma a campana, tuttavia intuitivamente saremmo portati a dire che la fascia diurna del giorno 3~settembre 2018 ha una distribuzione \emph{più} gaussiana rispetto a quella del giorno 25~settembre 2018.

Lo scopo dell'esercizio sarà formalizzare questa intuizione attraverso il test del \(\chi^{2}\) (chi quadrato).
In particolare il test è usato per due tipi di comparazione: \gof e quantificazione della differenza tra due variabili.
R mette a disposizione un'unica funzione, \mintinline{R}|chisq.test|, per effettuare entrambe le comparazioni: infatti stabilisce la comparazione in base agli argomenti e i loro tipi, ad esempio qualora l'argomento \mintinline{R}|x| è una matrice\footnote{Più propriamente chiamata matrice di contingenza.} allora \(\chi^{2}\) è un indice di contingenza.
Invece se \mintinline{R}|x| e \mintinline{R}|p| sono vettori numerici di medesima lunghezza, e contenuto differente, allora \mintinline{R}|chisq.test| calcolerà il p-dei-dati che sarà valutato a discrezione dell'utente.
In particolare la lunghezza dei vettori è il numero di intervalli in cui sono state divise le osservazioni, mentre le loro posizioni contengono rispettivamente il numero di osservazioni all'interno dell'intervallo \(k\) e la probabilità che la variabile aleatoria assuma un valore all'interno di tale intervallo.

Noi saremo meno generali di R e tratteremo solo l'aspetto di \gof.

\subsection{Accettazione o rifiuto dell'ipotesi nulla}
\label{subsec:h0-rej-accept}

Adesso che abbiamo un test\footnote{Non l'unico, altri sono possibili ad esempio quello di Kolmogorov-Smirnov.} applichiamolo alla variabile (continua) \(X\) delle fluttuazioni di velocità, quindi facciamo le seguenti ipotesi:
\begin{itemize}
	\item \(H_{0}\), la distribuzione di \(X\) è \(\symcal{N}(\mu_{X},\sigma_{X})\), e
	\item \(H_{1}\), la distribuzione di \(X\) \emph{non} è \(\symcal{N}(\mu_{X},\sigma_{X})\).
\end{itemize}
Queste sono due ipotesi composte perché la distribuzione attesa è definita sulla miglior stima del valore medio e della deviazione standard di \(X\), altrimenti sarebbero semplici.
Inoltre esse prendono rispettivamente il nome di ipotesi nulla o principale e alternativa.

Per decidere se accettare o rifiutare \(H_{0}\) è necessario fissare il livello di significatività \(\alpha\) e calcolare il p-dei-dati.
Il primo generalmente è fissato all'\(1\%\),~\(5\%\) o~\(10\%\) e rappresenta la probabilità di errore di prima specie, ovvero
\begin{equation*}
	\Prob{\text{rifiutare \(H_{0}\)}\given\text{\(H_{0}\) vera}} = \alpha
\end{equation*}
Il secondo è definito come la probabilità, supposta vera \(H_{0}\), di ottenere un \(\chi^{2}\) uguale o ``più estremo'' di quello effettivamente osservato.
Per comprendere meglio questa definizione consideriamo l'\cref{eq:chisq-formula}
\begin{equation}\label{eq:chisq-formula}
	\sum_{k=1}^{n}\frac{( O_{k}-E_{k})^{2}}{E_{k}}
\end{equation}
dove le \(N\) fluttuazioni di velocità sono state suddivise in \(n\) intervalli: per ciascun intervallo calcoliamo il numero di misure che vi cadono all'interno (\(O_k\)) e il numero \emph{atteso} di misure (\(E_k\)).
Notiamo che se ripetessimo l'intera serie di misure molte volte, allora il numero \(O_k\) di misure in ogni intervallo \(k\) può essere considerato come il risultato di un esperimento di conteggio e quindi dovrebbe seguire una distribuzione di Poisson.
Inoltre i molti diversi risultati per \(O_k\) dovrebbero avere un valore medio pari a \(E_k\) e vi dovrebbero fluttuare intorno con una deviazione standard dell'ordine di \(\smash[b]{\sqrt{E_k}}\)\thinspace.
Pertanto mettendo a rapporto ed elevando al quadrato la deviazione \(O_{k}-E_{k}\) e la dimensione delle sue fluttuazioni \(\smash[b]{\sqrt{E_k}}\)\thinspace, per ogni intervallo \(k\), otteniamo l'\cref{eq:chisq-formula}, che calcola il chi quadrato \emph{osservato} (\(\chi_{o}^{2}\)).
Quindi operativamente calcoleremo il p-dei-dati come \(\Prob{\chi^{2} > \chi_{o}^{2}} \): da notare l'uso del \emph{solo} simbolo \(>\) per indicare che questo è un test unilaterale destro.
Si tiene a precisare che il calcolo del p-dei-dati non è avvenuto usando il chi quadrato \emph{ridotto} (\(\tilde{\chi}^{2}\)), come riportato in~\cite{taylor1999introduzione}, questo perché R non mette a disposizione una funzione di ripartizione per il chi quadrato ridotto, ma solo per il \(\chi^{2}\) (vedi \mintinline{R}|pchisq|).

L'uso del p-dei-dati non era l'unica possibilità per decidere se accettare o rifiutare l'ipotesi nulla.
Un'altra possibilità è quella di utilizzare le regioni di rifiuto, tuttavia tale approccio richiede di calcolare la regione ogni qualvolta l'utente cambia il livello di significatività, ciò non accade con il p-dei-dati.
Esso può essere calcolato e poi valutato rispetto a diversi livelli di \(\alpha\).
Più precisamente il p-dei-dati e l'\(\alpha\) delineano due aree destre (``più estremo'') al disotto della \pdf del \(\chi^{2}\), in particolare l'area delineata da \(\alpha\) è la regione di rifiuto, in cui non deve cadere il \(\chi^{2}_{o}\) altrimenti l'ipotesi nulla è da rifiutare.
Ciò è equivalente a dire che l'area delineata dal p-dei-dati è contenuta all'interno di quella delimitata da \(\alpha\).
Per tanto se indichiamo con \(p\) il p-dei-dati si accetterà \(H_{0}\) se \(p > \alpha\).

\subsection{La definizione di intervalli}
\label{subsec:def-intervals}

Per rendere veramente operativa l'\cref{eq:chisq-formula} ci manca un ingrediente: il numero \(k\) di intervalli.
Come abbiamo già visto nel \Cref{sec:fluctuations} non è una scelta banale perché non esiste una vera e propria regola generale.
Perciò in questo esercizio, come nel precedente, il numero di intervalli può sia essere scelto dall'utente sia calcolato dallo script.

Per indicare il numero di intervalli si usano due opzioni: \mintinline{bash}|--bins| e \mintinline{bash}|--type|.
Il primo indica il numero di intervalli in cui dividere le misure raccolte.
Il secondo indica la tipologia di metodo per costruire gli intervalli: \mintinline{bash}|RGd| (default), \mintinline{bash}|Equal-Prob| o \mintinline{bash}|Equal-Size|.
Sostanzialmente con \mintinline{bash}|Equal-Prob| tutti gli intervalli hanno la medesima probabilità, ovvero stesso \(E_{k}=NP_{k}\), mentre con \mintinline{bash}|Equal-Size| tutti gli intervalli avranno stessa dimensione (eccetto pochi che avranno un basso numero di osservazioni); con entrambi i metodi è possibile specificare \mintinline{bash}|--bins|, che altrimenti di default vale~\(10\).

\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=39,lastline=56]{R}{chisq.R}
	\caption{Funzione \mintinline{R}{.bin.adjust}.}
	\label{fig:bin-adjust}
\end{figure}
L'opzione \mintinline{bash}|--bins| viene ignorata e il numero di intervalli è fissato a \(k=5+c\) dove \(c\) indica il numero di vincoli,\footnote{È il numero di parametri stimati, nel nostro caso sempre \(2\) perché per definire una normale abbiamo bisogno della media e della deviazione standard, che abbiamo stimato.} qualora non si specifichi alcun metodo oppure si scelga \mintinline{bash}|RGd|.
Successivamente usando entrambi i metodi precedenti calcoliamo i limiti per ciascun intervallo, che indicheremo con \(B_{i}^{0}\) per \mintinline{bash}|Equal-Prob| e \(B_{i}^{1}\) per \mintinline{bash}|Equal-Size|, dove \(i = 0, \dots, k\).
A questo punto fissiamo un peso \(\kappa=0,5\), o altro valore usando l'opzione \mintinline{bash}|--weight|, e calcoliamo gli intervalli per \mintinline{bash}|RGd| come segue
\begin{equation*}
	B_{i}^{\kappa}=(1-\kappa) B_{i}^{0}+\kappa B_{i}^{1}
	\quad
	0\leq\kappa\leq1
\end{equation*}
Si noti che per \(\kappa=0\) si ha \mintinline{bash}|Equal-Prob|, mentre per \(\kappa=1\) \mintinline{bash}|Equal-Size|.
Per ulteriori dettagli si rimanda a~\cite{Rolke2020,Rolke2020a}.

Quanto spiegato in linguaggio naturale è codificato nella funzione \mintinline{R}|.bin.fun| dello script \file{chisq.R}, la quale a sua volta chiama la funzione \mintinline{R}|.bin.adjust| (\Cref{fig:bin-adjust}) prima di terminare per verificare \(E_{k}\gtrsim5\).
Se tale condizione non è rispettata viene selezionato l'intervallo con il conteggio atteso più basso (\lref{min:lowcount}) e fuso con uno adiacente (\lrefrange{min:startfuse1}{min:endfuse1} e \lrefrange[nolabel]{min:startfuse2}{min:endfuse2}), così facendo si passa a \(k-1\) intervalli (\lref{min:remove1,min:remove2}), si ripete questo processo fino a quando tutti gli intervalli non rispettano la condizione (\lrefrange{min:startrepeat}{min:endrepeat}).
Tale controllo è necessario perché solo se tutti i numeri in gioco sono ragionevolmente grandi, il carattere discreto di \(O_{k}\) non è importante e la sua distribuzione di Poisson è ben approssimata dalla funzione di Gauss: infatti \(\Prob{\chi^{2} > \chi_{o}^{2}} \) tratta i numeri osservati \(O_{k}\) come variabili continue che sono distribuite attorno ai loro valori attesi \(E_k\) secondo una distribuzione gaussiana.
\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=123,lastline=135]{R}{chisq.R}
	\caption{Funzione \mintinline{R}{chi2gof}.}
	\label{fig:chi2gof}
\end{figure}

Prima di concludere questo \namecref{subsec:def-intervals} vale la pena capire perché è stato necessario suddividere le misure in intervalli: la ragione è da ricercarsi nella natura continua della variabile aleatoria \(X\) delle fluttuazioni di velocità.
Infatti come si è già studiato, per queste variabili non è definita una funzione di probabilità bensì una funzione di \emph{densità} di probabilità, che \emph{non} calcola una probabilità puntuale: infatti il suo codominio non è \(\interval{0}{1}\), come per la funzione di probabilità di una variabile discreta.
Tuttavia ogni valore che può assumere una variabile casuale ha una certa probabilità, ma questa è molto vicina a \(0\) e deve essere necessariamente così al fine di rispettare il secondo assioma di Kolmogorov.
Perciò nel caso volessimo usare la probabilità puntuale per calcolare \(E_k\) questo sarà molto vicino a \(0\), allora ricorriamo all'uso di intervalli.

\subsection[La funzione \texttt{chi2gof}]{La funzione \mintinline[fontsize=\large]{R}|chi2gof|}
\label{subsec:chi2gof}

Andremo ora a commentare la funzione \mintinline{R}|chi2gof| mostrata in \Cref{fig:chi2gof}.
Le \lref{min:bins,min:tmpbins} preparano il vettore \mintinline{R}|tmpbins| contenente i limiti di ogni intervallo.
Alla \lref{min:O} la funzione \mintinline{R}|cut| assegna a ciascun elemento di \mintinline{R}|x| uno dei \(k\) intervalli delimitati nel vettore \mintinline{R}|tmpbins|, pertanto il suo risultato sarà un vettore della medesima lunghezza di \mintinline{R}|x|, che sarà passato a \mintinline{R}|table|, la quale restituirà un oggetto omonimo contenente una tabella di contingenza, in cui ogni elemento è un intervallo con annesso il numero di elementi che vi cadono all'interno.

Ora che abbiamo il vettore \(O_k\), calcoliamo \(E_k\) come mostrato alla \lref{min:E}, in cui la funzione \mintinline{R}|pnull|, componente di \mintinline{R}|h0| (rappresentazione dell'ipotesi nulla, si veda anche \mintinline{R}|.hyp.gauss|), calcola il vettore \(P_k\) sfruttando la funzione di ripartizione della normale (\mintinline{R}|pnorm|) ed effettuando le sottrazioni con \mintinline{R}|diff|, in pratica considerando il generico intervallo \(\linterval{a}{b}\) stiamo calcolando \(\Prob{a < X \leq b} = F(b)-F(a)\).

Proseguendo, alla \lref{min:df} calcoliamo i gradi di libertà: ovvero sottraiamo il numero di vincoli dai dati osservati (nel nostro caso il numero \(n\) di intervalli): in particolare abbiamo un vincolo dovuto al numero \(N\) di osservazioni, usato per ricavare \(E_k\) e che di fatto abbiamo calcolato con \(\text{\mintinline{R}|length(x)|}=\sum_{k=1}^{n}O_k\), più due vincoli dovute alle stime, come abbiamo già accennato.

Infine alle \lrefrange{min:startretchi2gof}{min:endretchi2gof} la funzione \mintinline{R}|chi2gof| restituisce una lista contenente
diverse componenti tra cui il p-dei-dati indicato come \mintinline{R}|p.value| (\lref{min:pvalue}).
Esso è calcolato, come già detto, sfruttando la funzione di ripartizione del \(\chi^2\): in particolare l'argomento \mintinline{R}|lower.tail = FALSE| calcola \(1-\Prob{\chi^{2}\leq\chi^{2}_{o}} \).

Il risultato di questa funzione verrà stampato da \mintinline{R}|print.chi2gof| che non verrà presentata, ma potrà essere approfondita dal lettore interessato.

\subsection{Risultati}
\label{subsec:ris-chisq}

Siamo ora pronti a rispondere alla domanda di apertura di questo lungo paragrafo, pertanto in una nuova finestra di terminale eseguiamo
\begin{minted}[linenos=false]{text}
	$ ./chisq.R --daynight settembre/2018-09-03.dat

	          Chi-squared test for given distribution

	  data:  daytime
	  X-squared = 11.266, df = 4, p-value = 0.02373
	  ---------
	  data:  nightime
	  X-squared = 15.757, df = 4, p-value = 0.003363

	$ ./chisq.R settembre/2018-09-25.dat

	          Chi-squared test for given distribution

	  X-squared = 20.437, df = 4, p-value = 0.0004094

\end{minted}
Cominciamo con il porre il livello di significatività al \(5\%\), osserviamo i \mintinline{text}|p-value| e visto che sono tutti inferiori ad \(\alpha\) diciamo che il disaccordo è \emph{significativo} e rigettiamo l'ipotesi nulla.
Tuttavia se poniamo \(\alpha=1\%\) notiamo che il p-dei-dati della fascia diurna del giorno 3~settembre 2018 è maggiore dell'\(1\%\) quindi accettiamo l'ipotesi nulla, ma solo in questo caso negli altri dovremo rifiutarla dicendo che il disaccordo è \emph{altamente significativo}.

Infine si noti che le fluttuazioni di velocità per la fascia diurna del giorno 3~settembre 2018 sono le uniche con distribuzione gaussiana, questo è stato verificato usando gli script scritti in Bash presenti nella cartella \file{tests}, i quali hanno permesso di calcolare il p-dei-dati per diversi periodi temporali: in particolare si è applicato lo script \file{chisq.R}, con e senza l'opzione \mintinline{bash}|--daynight|, a finestre incrementali di giorni successivi, ovvero prima un giorno, poi due, tre, quattro e così via fino all'intero mese.
Tuttavia tale script non è più immediatamente operativo a meno che non si cambi l'output della funzione \mintinline{R}|print.chi2gof|.
