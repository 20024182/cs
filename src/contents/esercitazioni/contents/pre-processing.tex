% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\begin{figure}
	\centering
	\pathinputminted[escapeinside=££,firstline=16,lastline=44,breakafter={,}, showspaces, space=~]{R}{read_dataset.R}
	\caption{Funzione \mintinline{R}{read_dataset}.}
	\label{fig:read-dataset}
\end{figure}

\section{Pretrattamento del dato}
\label{sec:pre-processing}

Iniziamo con l'esaminare come sono stati importati i \dataset considerando la funzione\linebreak\mintinline{R}|read_dataset| mostrata in \Cref{fig:read-dataset} e contenuta in \file{src/utils/read\string_dataset.R}.
Essa presa in input il percorso di un \mintinline{R}|file| lo passa a \mintinline{R}|scan| per poter ottenere un \mintinline{R}|data.frame| in cui ogni colonna\footnote{Nel seguito useremo questo termine per riferirci a un carattere statistico. In altri contesti, come nel \textit{\textenglish{data mining}}, si parla di variabile, mentre in apprendimento automatico di \textit{\textenglish{feature}}.} ha un tipo e un'etichetta (\lrefrange{min:scanstart}{min:scanend}).
Alla \lref{min:scanend} viene utilizzato un operatore di pipe, che permette di far ``fluire'' l'output di \mintinline{R}|scan| all'interno della funzione \mintinline{R}|unite|, che unisce le prime due colonne, corrispondenti a data e ora, in una nuova colonna chiamata \mintinline{R}|"datetime"| eliminando le due originali.
Tutto ciò è necessario per poter usare la funzione \mintinline{R}|complete| (\lrefrange{min:completestart}{min:completeend}), che assicura di riordinare le righe\footnote{Ancora una volta usiamo un termine ``rozzo'', ma semplice per indicare una generica unità statistica.} e inserire quelle mancanti, ovviamente assumendo ogni colonna \mintinline{R}|NA|, \emph{per ora}.
Infine alla \lref{min:rownames} si imposta per ogni riga un'etichetta contenente l'orario nel formato \mintinline{text}|ora:minuti| e successivamente si elimina la colonna \mintinline{R}|"datetime"| (\lref{min:rmdatetime}).
\begin{figure}
	\centering
	\pathinputminted[escapeinside=££,firstline=50,lastline=54,breakafter={,}, showspaces, space=~]{R}{read_dataset.R}
	\caption{Funzione \mintinline{R}{.impute}.}
	\label{fig:.impute}
\end{figure}
Alla \lref{min:ret} viene restituita una lista formata da quattro componenti: \mintinline{R}|day| è una stringa contente il giorno (unico all'interno di un \dataset) in cui sono avvenute le misure, \mintinline{R}|df| è l'imputazione del risultato delle manipolazioni dell'oggetto ritornato da \mintinline{R}|scan|, mentre \mintinline{R}|sunrise| e \mintinline{R}|sunset|, componenti della lista restituita da \mintinline{R}|.ephemeris|, sono rispettivamente gli orari in cui è sorto e tramontato il sole nel giorno \mintinline{R}|day|.

La realizzazione di \mintinline{R}|read_dataset| ha richiesto diverse funzioni non presenti in R: infatti \mintinline{R}|unite| e \mintinline{R}|complete| provengono dal pacchetto \mintinline{R}|tidyr|~\cite{Wickham2022}, mentre \mintinline{R}|.ephemeris| usa \mintinline{R}|sunriset| dal pacchetto \mintinline{R}|maptools| e, come si vedrà nel prossimo \namecref{subsec:imputation}, \mintinline{R}|.impute| si affida a \mintinline{R}|na_kalman| proveniente da \mintinline{R}|imputeTS|~\cite{Moritz2017}.

In ultimo è importante sottolineare che sono stati condotti con successo alcuni test di validità utilizzando \dataset malformati: ovvero in cui erano presenti più giorni oppure si usavano formati invalidi per data e ora.

\subsection{Il processo di imputazione}
\label{subsec:imputation}

\begin{figure}[b]
	\captionsetup{skip=6pt}
	\centering
	\subcaptionbox{\label{subfig:imput-mean-temp}}
	[.49675\textwidth]{\includegraphics[width=\linewidth]{imput-mean-temp.pdf}}
	\subcaptionbox{\label{subfig:imput-kalman-temp}}
	[.49675\textwidth]{\includegraphics[width=\linewidth]{imput-kalman-temp.pdf}}
	\caption{Imputazione sulla serie temporale \mintinline{text}{TemperaturaAria} del giorno 28~settembre 2018 con due differenti algoritmi: \subref*{subfig:imput-mean-temp} \mintinline{R}{na_mean}, e \subref*{subfig:imput-kalman-temp} \mintinline{R}{na_kalman}.}\label{fig:ggplot-na-temp}
\end{figure}

In \Cref{fig:.impute} è riportato il codice della funzione \mintinline{R}|.impute| incontrata precedentemente alla \lref{min:ret}.
L'uso dell'imputazione ha permesso di sostituire i valori mancanti, introdotti da \mintinline{R}|complete| (\lrefrange{min:completestart}{min:completeend}), all'interno del \dataset.
Tale processo era necessario \emph{solo} nella soluzione dell'esercizio presentato nel \Cref{sec:lagrangian}.
Tuttavia, pur introducendo un rallentamento nelle performance, si è deciso comunque di adottare questo approccio anche nelle altre soluzioni al fine di poter lavorare sempre sugli stessi \dataset.

Innanzitutto l'algoritmo di imputazione usato è \textenglish{ARIMA State Space Representation \& Kalman Sm.} (\mintinline{R}|na_kalman|) presente nel pacchetto \mintinline{R}|imputeTS|.
In particolare la scelta dell'algoritmo è avvenuta seguendo i consigli proposti dal creatore del pacchetto: infatti essa non è l'unica.
Altre possibili alternative sono quelle di sostituire i valori mancanti in una data colonna con la media, la mediana o la moda calcolate sui valori disponibili.
Ad ogni modo non sempre questi sono gli approcci migliori, come mostreremo nel prossimo esempio.

Consideriamo la \Cref{fig:ggplot-na-temp} che mette a confronto gli algoritmi delle funzioni \mintinline{R}|na_mean| e \mintinline{R}|na_kalman|.
È immediato osservare la differenza tra i due approcci: infatti in \Cref{subfig:imput-mean-temp} i valori imputati (in rosso) non seguono il trend, al contrario di quelli imputati dall'algoritmo di \Cref{subfig:imput-kalman-temp}.
Questo è causato dal fatto che la serie temporale presenta un forte trend a differenza di quella mostrata in \Cref{fig:ggplot-na-speed}, in cui i due approcci risultano \emph{abbastanza} alternativi.

Infine si vuole ricordare che R mette di base a disposizioni differenti algoritmi di imputazione, ma si è preferito cercare un pacchetto che mettesse a disposizione lo stato dell'arte degli algoritmi di imputazione per \emph{serie temporali} univariate.
In particolare si considera ogni colonna di un \dataset una serie temporale, ma nelle esercitazioni, tranne quella del \Cref{sec:lagrangian}, si considereranno \emph{variabili aleatorie}.

\begin{figure}[t]
	\captionsetup{skip=6pt}
	\centering
	\subcaptionbox{\label{subfig:imput-mean-speed}}
	[.4967\textwidth]{\includegraphics[width=\linewidth]{imput-mean-speed.pdf}}
	\subcaptionbox{\label{subfig:imput-kalman-speed}}
	[.4967\textwidth]{\includegraphics[width=\linewidth]{imput-kalman-speed.pdf}}
	\caption{Imputazione sulla serie temporale \mintinline{text}{VelocitaVento} del giorno 28~settembre 2018 con due differenti algoritmi: \subref*{subfig:imput-mean-speed} \mintinline{R}{na_mean}, e \subref*{subfig:imput-kalman-speed} \mintinline{R}{na_kalman}.}\label{fig:ggplot-na-speed}
\end{figure}
