% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\section{L'equazione di Langevin}
\label{sec:langevin}

In quest'ultima esercitazione ci proponiamo di tracciare le traiettorie delle particelle di inquinanti rilasciate in atmosfera, in condizioni turbolente.
Per fare ciò utilizzeremo un modello lagrangiano a particelle basato sulle equazioni di Langevin.

Secondo Reynolds, la velocità del vento è la somma di due velocità che si sovrappongono: ovvero la velocità media e quella turbolenta.
Tuttavia in condizioni di turbolenza solo la componente media è conosciuta, quella turbolente deve essere modellata attraverso un'equazione stocastica differenziale, nota come equazione di Langevin, che per una sola variabile assume la forma
\begin{equation}\label{eq:langevin}
	\diff u_{i}(t) = a_i(\symbf{u},\symbf{x})\diff t + b_{ij}(\symbf{u},\symbf{x})\diff W_{j}
\end{equation}
essa sarà accoppiata con l'equazione per lo spostamento
\begin{equation}\label{eq:trajectory}
	\diff x_{i}(t) = u_{i}(t)\diff t
\end{equation}
Integrando numericamente le \cref{eq:langevin,eq:trajectory} è possibile conoscere la posizione di una particella all'istante \(t\).
Ad ogni modo per procedere correttamente dobbiamo considerare la velocità e la posizione della particella come un processo markoviano bivariato, in cui il presente è correlato con il passato e il futuro con il presente, ma passato e futuro sono staticamente indipendenti.
Per fare ciò si dovrà scegliere opportunamente il termine \(\diff W_{j}\).

\subsection{Un caso semplice}
\label{sec:a-simple-case}

L'\cref{eq:langevin} non può essere direttamente utilizzata: infatti vi sono vari termini, come \(a\), \(b\) e \(\diff W\), che non sono stati esplicitati.
Tuttavia prima di definirli formalmente è necessario fare alcune assunzioni sulla turbolenza: ovvero considerandola stazionaria, omogenea, isotropa e \emph{gaussiana}.
Tali considerazioni permettono di ridurre la \labelcref{eq:langevin} a
\begin{equation}\label{eq:langevin-integrable}
	\diff u(t) = -\frac{C_{0}\bar{\epsilon}}{2\sigma_{u}^2}u\diff t + \sqrt{C_{0}\bar{\epsilon}}\diff W
\end{equation}
da cui notiamo che sono stati rimossi i pedici \(i\) e \(j\), perché consideriamo solo la componente verticale della velocità, ottenendo di fatto un modello unidimensionale.
Inoltre grazie alle assunzioni fatte sulla turbolenza abbiamo potuto fare le seguenti sostituzioni
\begin{equation*}
	a = -\frac{C_{0}\bar{\epsilon}}{2\sigma_{u}^2}
	\qquad
	b = \sqrt{C_{0}\bar{\epsilon}}
\end{equation*}
in cui \(C_{0}\) è una costante universale il cui valore può variare, a seconda degli autori, tra~\(2\) e \(7\), \(\epsilon\) è il tasso medio di dissipazione dell'energia cinetica turbolenta e \(\sigma_{u}^2\) è la varianza della velocità.
Invece il termine \(\diff W\) è un incremento in un processo di Weiner con media zero e varianza \(\diff t\), per il quale vale \(\tau_{\eta}\ll\diff t\ll T_{L}\) dove \(\tau_{\eta}\) è il tempo di scala di Kolmogorov e \(T_{L}\) è, come abbiamo visto nel \Cref{sec:lagrangian}, il tempo di scala lagrangiano.
Infine sottolineiamo che \(C_{0}\bar{\epsilon}\) è legato al tempo di scala lagrangiano dalla seguente relazione \(2\sigma_{u}^2\divslash T_{L}\).

\subsection[La funzione \texttt{solve\_langevin\_eq}]{La funzione \mintinline[fontsize=\large]{R}|solve_langevin_eq|}
\label{subsec:solve-langevin-eq}

L'\cref{eq:langevin-integrable}, come abbiamo già detto, se integrata fornisce la velocità turbolenta di una particella all'istante \(t\).
Tuttavia essa può anche essere \textit{discretizzata} in incrementi temporali di \(\increment t\) per calcolare numericamente l'evoluzione di \(u\).
Per tanto riscriviamola come segue
\begin{equation}\label{eq:langevin-discrete}
	u^{t+\increment t} = u^{t} - \frac{C_0\bar{\epsilon}}{2\sigma_{u}^{2}} u^{t}\increment t + \sqrt{C_0\bar{\epsilon}}\increment W = u^{t} - \frac{\increment t}{T_{L}} u_{t} + \sqrt{\frac{2\sigma_{u}^2}{T_{L}}}\increment W = u^{t} \biggl( 1 - \frac{\increment t}{T_{L}}\biggr) + \sqrt{\frac{2\sigma_{u}^2}{T_{L}}}\increment W
\end{equation}
e ora non resta che formalizzare i termini \(\increment t\) e \(\increment W\), trascurando \(T_{L}\) perché già ampiamente discusso.
Il primo deve essere calcolato considerando, quanto detto nel \namecref{sec:a-simple-case} precedente su \(\diff t\), quindi si è scelto di calcolarlo come \(\increment t = 0,05 T_{L}\) (vedi~\cite{Thomson1987}), così da garantire \(\increment t \ll T_{L}\): ovvero \(\increment t\) è sufficientemente piccolo da assicurare che, nel frattempo, una particella non cambi posizione.
Invece \(\increment W\) può essere calcolato estraendo un numero da una distribuzione gaussiana standardizzata e moltiplicandolo per \(\smash[t]{\sqrt{\increment t}}\).
Infine si vuol fare notare che nell'\cref{eq:langevin-discrete} decomponiamo la velocità turbolenta in due componenti: una correlata con la velocità all'istante precedente e la seconda estratta da un processo di Wiener.

\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=51,lastline=67]{R}{langevin.R}
	\caption{Funzione \mintinline{R}{solve_langevin_eq}.}
	\label{fig:solve-langevin-eq}
\end{figure}
Prima di concludere questo \namecref{sec:langevin} commentiamo la funzione \mintinline{R}|solve_langevin_eq| in \Cref{fig:solve-langevin-eq} soffermandoci sulle \lref{min:velocities,min:trajectories}.
Nella prima si nota che nel secondo argomento della funzione \mintinline{R}|replicate| sfruttiamo la \labelcref{eq:langevin-discrete} con alcuni accorgimenti algebrici; mentre nella seconda, nel corpo funzione anonima, troviamo una riscrittura della \labelcref{eq:trajectory}
\begin{equation*}
	x^{t+\increment t} = x^{t} + u^{t+\increment t}\increment t
\end{equation*}
Inoltre alle \lref{min:velocitiesinit,min:trajectories-init} si può notare che al tempo \(t = 0\) abbiamo \(x^{t_{0}} = 0\) e \(u^{t_{0}} = 0\).

Concludiamo con una dimostrazione pratica eseguendo lo script \file{langevin.R}
\begin{figure}
	\centering
	\begin{subcaptionblock}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{20180919-velocities.pdf}
		\phantomcaption
	\end{subcaptionblock}
	\begin{subcaptionblock}{\textwidth}
		\centering
		\includegraphics[width=\linewidth]{20180919-trajectories.pdf}
		\phantomcaption
	\end{subcaptionblock}
	\caption{Esempi di grafici prodotti da \file{langevin.R}.}
	\label{fig:trajectories-velocities}
\end{figure}
\begin{minted}[linenos=false]{bash}
	./langevin.R settembre/2018-09-19.dat 2000 130 -s 12345
\end{minted}
che produrrà i grafici di \Cref{fig:trajectories-velocities} e il seguente output
\begin{minted}[linenos=false]{text}
Mean of Turbulence Velocities = 0.004007339
Std. Dev. of Turbulence Velocities = 0.3189957
Std. Dev. of Total Velocities = 0.3281253
\end{minted}
da cui osserviamo che la media delle velocità turbolente è \(0\), come ci si aspettava visto che abbiamo una turbolenza gaussiana con media \(0\).
Inoltre osserviamo che l'utente può replicare gli output dello script con l'opzione \mintinline{bash}|-s| che permette di impostare un seme iniziale per la funzione \mintinline{R}|rnorm| di \lref{min:velocities}.
