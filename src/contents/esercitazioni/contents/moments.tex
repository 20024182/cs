% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\section{Calcolo dei momenti}
\label{sec:moments}

Ricordiamo alcune definizioni già date durante il corso.
\begin{defn}\label{def:moments}
	Dato il campione \(x_{1}, x_{2}, \dots, x_{n}\) possiamo calcolare il \emph{momento semplice campionario} di ordine \( k \) come
	\begin{equation}\label{eq:sample-raw-mom}
		\hat{m}_{k}^{} = \frac{1}{n}\sum_{i=1}^{n}x_{i}^{k}
	\end{equation}
	Il \emph{momento centrale campionario} di ordine \(k\) è la media aritmetica delle \(k\)-esime potenze degli scarti dalla media
	\begin{equation}\label{eq:sample-central-mom}
		\hat{\mu}_{k} = \frac{1}{n}\sum_{i=1}^{n}(x_{i}-\bar{x})^{k}
	\end{equation}
\end{defn}
Si noti che l'\cref{eq:sample-raw-mom} calcola una stima corretta (\textit{\textenglish{unbiased}}), al contrario dell'\cref{eq:sample-central-mom} che calcola una stima non corretta (\textit{\textenglish{biased}}).
Le \cref{eq:sample-raw-mom,eq:sample-central-mom} si possono riscrivere l'una in funzione dell'altra utilizzando la formula di Newton\footnote{Esprime lo sviluppo della potenza \(n\)-esima di un binomio qualsiasi,\begin{equation*}
		(a+b) ^{n} = \sum_{k=0}^{n}\binom{n}{k}a^{k}b^{n-k}=\sum_{k=0}^{n}\binom{n}{k}a^{n-k}b^{k}
		\quad
		\text{per ogni intero \(n\geq0\), con \(a,b\in\symbb{R}\)}
\end{equation*} in cui il fattore \(\binom{n}{k}\) rappresenta il coefficiente binomiale ed è sostituibile con \(\frac{n!}{k!(n-k)!}\).} e la proprietà di linearità del valore atteso.
\begin{equation}\label{eq:sample-raw-mom-binom}
	\hat{m}_{k} = \sum_{j=0}^{k}\binom{k}{j}\hat{\mu}_{j} \bar{x} ^{k-j}
\end{equation}
\begin{equation}\label{eq:sample-central-mom-binom}
	\hat{\mu}_{k} = \sum_{j=0}^{k}\binom{k}{j}\hat{m}_{j}(-\bar{x})^{k-j}
\end{equation}

Lo scopo dell'esercizio è verificare le \cref{eq:sample-raw-mom,eq:sample-raw-mom-binom}, \labelcref{eq:sample-central-mom,eq:sample-central-mom-binom}.

\subsection[La funzione \texttt{get\_moment}]{La funzione \mintinline[fontsize=\large]{R}|get_moment|}
\label{subsec:get-moment}

\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=28,lastline=38]{R}{moments.R}
	\caption{Funzione \mintinline{R}{get_moment}.}
	\label{fig:get-moment}
\end{figure}

In \Cref{fig:get-moment} viene presentata la funzione \mintinline{R}|get_moment| contenuta nel file \file{moments.R}, che non verrà commentato integralmente poiché il resto del suo contenuto è solo di contorno.
Ad ogni modo il lettore interessato può sempre leggere il file sopracitato, oltre che provarlo.

Alla \lref{min:getmomentifbinom} si stabilisce se è necessario calcolare il momento usando l'\cref{eq:sample-raw-mom-binom} o \labelcref{eq:sample-central-mom-binom}, altrimenti si usa \labelcref{eq:sample-raw-mom} o \labelcref{eq:sample-central-mom}.
Medesimo controllo avviene alla \lref{min:getmomentifcenter}, in cui si stabilisce se usare l'\cref{eq:sample-raw-mom} o \labelcref{eq:sample-central-mom}.
Tuttavia il cuore della funzione è rappresentato dalle \lref{min:getmomentsimple, min:getmomentbinom}.
La prima non è nient'altro che una fedele codifica dell'\cref{eq:sample-raw-mom}.
Per quanto riguarda la seconda va solo sottolineato come funziona \mintinline{R}|sapply|.
Essa prende in input un vettore, nel nostro caso di interi, e applica ad ogni elemento una funzione, che in \Cref{fig:get-moment} è anonima.

Infine si fa notare che nella \lref{min:getmomentbinom} si è usato un oggetto di tipo \mintinline{R}|logical| all'interno di un'espressione matematica.
Questo perché, come il C, anche R tratta i \mintinline{R}|logical|, come 0 (\mintinline{R}|FALSE|) e 1 (\mintinline{R}|TRUE|).

\subsection{Test e risultati}
\label{subsec:test-risultati}

La \Cref{fig:order-1-vs-2} riporta due esempi di output, usati come test, dello script \file{moments.R} e ottenibili rispettivamente con
\begin{minted}[linenos=false]{bash}
	$ ./moments.R 1 'TemperaturaAria' settembre/*.dat
	$ ./moments.R 2 'TemperaturaAria' settembre/*.dat
\end{minted}
In generale come test si sono verificate le seguenti relazioni:
\begin{itemize}
	\item \(\mu_{0}=m_{0}=1\);
	\item \(m_{1}=\bar{x}\), ovvero il momento semplice di ordine~\(1\) è la media;
	\item \(\mu_{1}=0\), ovvero il momento centrale di ordine~\(1\) è la media degli scarti dalla media, che è ovviamente 0;
	\item \(\mu_{2}=\sigma^{2}\), ovvero il momento centrale di ordine~\(2\) è la varianza;
	\item \(m_{2}=\bar{x}^{2}+\sigma^{2}\);
	\item \(\mu_{3}=m_{3}-3\bar{x}m_{2}+2\bar{x}^3\);
	\item \(m_{3}=\mu_{3}+3\bar{x}\sigma^{2}+\bar{x}^{3}\).
\end{itemize}

Tuttavia, la figura non mostra i risultati attesi, questo perché:
\begin{itemize}
	\item le \crefrange{eq:sample-raw-mom}{eq:sample-central-mom-binom} calcolano una stima del momento che, come tutto in statistica, è affetta da un errore quantificabile con un intervallo di confidenza;
	\item inoltre, per quanto accurato sia R, la rappresentazione interna di un valore reale è arrotondata\footnote{Tutti i numeri sono internamente arrotondati a 53 bit.} e le operazione aritmetiche propagano tale errore.
\end{itemize}
Tali aspetti non sono da sottovalutare e spiegano perché in \Cref{subfig:order-1} il componente \mintinline{R}|$binom.central| rispetta il valore atteso, al contrario di \mintinline{R}|$central| che si avvicina (molto) a \(0\).
Per evidenziare la presenza di un errore tra \labelcref{eq:sample-raw-mom,eq:sample-raw-mom-binom} (risp. \labelcref{eq:sample-central-mom,eq:sample-central-mom-binom}) si è deciso di riportare l'errore relativo commesso (\lref{min:rawrelerror,min:centralrelerror}) calcolato come il rapporto tra la differenza in valore assoluto di \labelcref{eq:sample-raw-mom,eq:sample-raw-mom-binom} (risp. \labelcref{eq:sample-central-mom,eq:sample-central-mom-binom}) e \labelcref{eq:sample-raw-mom} (risp. \labelcref{eq:sample-central-mom}).
\begin{figure}[t]
	\centering
	\begin{subcaptionblock}{.4955\textwidth}
		\centering
		\pathinputminted[escapeinside=££]{text}{order-1.txt}
		\caption{\label{subfig:order-1}}
	\end{subcaptionblock}
	\begin{subcaptionblock}{.4955\textwidth}
		\centering
		\pathinputminted[linenos=false]{text}{order-2.txt}
		\caption{\label{subfig:order-2}}
	\end{subcaptionblock}
	\caption{Calcolo dei momenti di ordine \(k\) della variabile \mintinline{text}{TemperaturaAria} per il mese di settembre 2018: \subref*{subfig:order-1} \(k = 1\), e \subref*{subfig:order-2} \(k = 2\). }
	\label{fig:order-1-vs-2}
\end{figure}

Infine va osservato che in \Cref{subfig:order-2} non vi è corrispondenza tra \mintinline{R}|$var| e \mintinline{R}|$central|, questo è dovuto al fatto che la funzione \mintinline{R}|var|, che calcola \mintinline{R}|$var|, usa \(n-1\) gradi di libertà fornendo una stima della varianza corretta.