% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\section{\pdf delle fluttuazioni di velocità}
\label{sec:fluctuations}

Come già sappiamo, data una variabile aleatoria continua \(X\) e la sua \pdf \(f_{X}(x)\) vale quanto segue
\begin{equation}\label{eq:pdf-sum-up-1}
	\int_{-\infty}^{+\infty} f_{X}(x)\diff x = 1
\end{equation}
L'\cref{eq:pdf-sum-up-1} ci dice che l'area compresa tra la \pdf e l'asse delle ascisse è uguale a~\(1\).
Dall'analisi sappiamo che l'area al disotto di una curva può essere approssimata per eccesso (risp.\ difetto) dall'area del plurirettangolo circoscritto (risp.\ inscritto) tra la curva e l'asse delle ascisse.

Lo scopo dell'esercizio è verificare graficamente quanto enunciato.
In particolare per fare ciò si è realizzato un istogramma in cui sull'asse delle ascisse troviamo le fluttuazioni di velocità divise in intervalli, mentre sull'asse delle ordinate abbiamo la densità stimata di probabilità.
Inoltre sullo stesso grafico è stata plottata una stima della \pdf ricava dalla funzione R \mintinline{R}|density|, che utilizza il metodo della \textenglish{kernel density estimation} (\mbox{KDE}~\cite{enwiki:1100268813}).

\subsection{Risultati grafici}
\label{subsec:ris-fluctuations}

Ora che abbiamo illustrato lo scopo vale la pena provare lo script \file{fluctuations.R}, per questo motivo mostriamo alcune \emph{sue esclusive} opzioni.
Esaminando l'help (\mintinline{bash}|-h|) dello script si trovano tre opzioni non ancora discusse: \mintinline{bash}|--labels|, \mintinline{bash}|--bw| e \mintinline{bash}|-b|.
La prima aggiunge sopra ad ogni barra dell'istogramma la corrispettiva densità stimata di probabilità, ovvero aggiunge qualche dettaglio al grafico.
\begin{figure}[t]
	\captionsetup{skip=6pt}
	\centering
	\begin{subcaptionblock}{.48875\textwidth}
		\centering
		\includegraphics[width=\linewidth]{20180903-daytime.pdf}
		\phantomcaption
	\end{subcaptionblock}
	\begin{subcaptionblock}{.48875\textwidth}
		\centering
		\includegraphics[width=\linewidth]{20180925.pdf}
		\phantomcaption
	\end{subcaptionblock}
	\caption{Esempi di grafici prodotti da \file{fluctuations.R}.}
	\label{fig:examples-fluctuations}
\end{figure}

La seconda permette di modificare il parametro \textenglish{bandwidth} del \textenglish{kernel} in modo da comportare un cambiamento di scala.
In effetti questo parametro ha il significato di deviazione standard: perché la ``reale'' deviazione standard di un \emph{qualunque} \textenglish{kernel} è sempre fissa a 1 per una questione di semplicità.
Visivamente la modifica di questo parametro permette di adattare meglio la curva di densità stimata rispetto ai dati.
Tuttavia qualora il suo valore fosse troppo piccolo allora si verificherebbe un fenomeno di \textenglish{overfitting}, al contrario con un valore troppo grande si avrebbe \textenglish{underfitting} (eccessiva distorsione).
Vista la scelta cruciale del parametro si è deciso di permettere all'utente sia di inserire un numero sia di indicare una funzione che ne calcoli un valore.
Le funzioni supportate nativamente da R sono: \mintinline{R}|nrd0| (default), \mintinline{R}|nrd|, \mintinline{R}|ucv|, \mintinline{R}|bcv|, \mintinline{R}|sj-ste| e \mintinline{R}|sj-dpi|.

Simile all'opzione precedente, \mintinline{bash}|-b| permette sia di indicare il numero di intervalli in cui suddividere le misure sia il nome di una funzione che calcoli tale numero.
Anche questa volta abbiamo una scelta cruciale da fare: infatti se il numero \(k\) fosse troppo grande allora l'istogramma sarebbe distorto (a causa del rumore) con la possibilità di esibire caratteristiche non veritiere; al contrario se \(k\) fosse troppo piccolo allora l'istogramma maschererebbe alcune caratteristiche significative (si pensi al caso estremo \(k = 1\)).
Per tale ragione la scelta di \(k\) dovrebbe procedere per \textit{trial and error} oppure si possono usare alcune regole generali codificate in R: \mintinline{R}|sturges| (default), \mintinline{R}|freedman-diaconis| o \mintinline{R}|scott|.

In \Cref{fig:examples-fluctuations} sono mostrati due esempi di grafici plottati dallo script e ottenuti rispettivamente con
\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=69,lastline=88]{R}{fluctuations.R}
	\caption{Funzione \mintinline{R}{draw_plot}.}
	\label{fig:draw-plot}
\end{figure}
\begin{minted}[linenos=false]{bash}
	$ ./fluctuations.R settembre/2018-09-03.dat --daynight \
	    -t '03 settembre 2018'
	$ ./fluctuations.R settembre/2018-09-25.dat -t '25 settembre 2018'
\end{minted}
Si osservi che se il numero di misure e intervalli \(k\) tendessero contemporaneamente all'infinito si avrebbe una sovrapposizione dell'istogramma con la curva.
Tuttavia qualora non si disponesse di infinite misure allora l'eccessiva crescita di \(k\) porterebbe ad avere un istogramma le cui barre non sarebbero più \emph{continue}: ovvero ci sarebbero degli spazi tra di loro.
Ciò comporterebbe che l'istogramma non rappresenti più l'area al disotto della \pdf.

Riprenderemo la \Cref{fig:examples-fluctuations} nel \Cref{sec:chisq}, in cui mostreremo, con il test chi quadrato, che solo una delle due distribuzioni è gaussiana.


\subsection[La funzione \texttt{draw\_plot}]{La funzione \mintinline[fontsize=\large]{R}|draw_plot|}
\label{subsec:draw-plot}

In questo \namecref{subsec:draw-plot} andremo a esaminare la funzione \mintinline{R}|draw_plot| in \Cref{fig:draw-plot}.
Innanzitutto l'argomento \mintinline{R}|bw| alla \lref{min:parambw} ha come valore di default lo stesso del suo omonimo, ma nella funzione \mintinline{R}|density.default|.
Si è preferito questo approccio piuttosto che riportare direttamente il valore perché nel caso in cui in futuro dovesse cambiare nella funzione R anche \mintinline{R}|draw_plot| si adatterebbe senza alcun ulteriore intervento.

Tralasciando le funzioni ausiliarie alle \lref{min:ignoreparsebw,min:ignoreparsebreaks}, consideriamo il blocco formato dalle \lrefrange{min:r}{min:ylim}.
Prima di spiegare l'utilità della doppia chiamata alla funzione \mintinline{R}|hist|, spieghiamo la sua logica: inizialmente essa calcola il range dell'asse delle ascisse come \mintinline{R}|xlim = range(breaks)|, poi chiama la funzione \mintinline{R}|plot.histogram| che calcola il range dell'asse delle ordinate come \mintinline{R}|ylim <- range(counts/(length(x) * as.double(diff(breaks))), 0)| (per dettagli vedere \Cref{subsec:den-vs-freq}), prepara lo ``spazio'' dove plottare e infine plotta le barre chiamando \mintinline{R}|rect|.
Alla \lref{min:linesden} si usa la funzione di basso livello \mintinline{R}|lines| per aggiungere la curva della \pdf stimata.
Tuttavia l'aggiunta non comporta un ricalcolo di \mintinline{R}|xlim| e \mintinline{R}|ylim|, pertanto è importante precalcolare i range degli assi prima di plottare l'istogramma (\lref{min:xlim,min:ylim}).

Inoltre l'istogramma plottato avrà bordi con lo stesso colore del contenuto delle barre ovvero \mintinline{R}|"cyan"|: infatti si vuole solo evidenziare il bordo esterno dell'istogramma e ciò è reso possibile dalle \lrefrange{min:lines-start}{min:linesend}.
Tale decisione mette in risalto la forma dell'istogramma ed è consistente con l'obbiettivo di massimizzare il rapporto ``\textenglish{\textit{data-to-ink}}'' di Tufte~\cite{Tufte1983}.

Infine la \lref{min:polygon} colora l'area al disotto della curva della \pdf stimata.

\subsection{Densità \textit{versus} frequenza}
\label{subsec:den-vs-freq}

Alla \lref{min:prob} viene richiesto che sull'asse delle ordinate ci sia la densità stimata di probabilità e non le frequenze (\mintinline{R}|prob = TRUE|).
\begin{defn}
	Sia \(X\) un \dataset (variabile aleatoria) di \(n\) misure reali.
	Consideriamo un intervallo qualunque \(\symcal{B}\) di numeri reali di lunghezza \(\delta\).
	Sia \(F\) il numero di misure che appartengono a \(X\) e cadono all'interno di \(\symcal{B}\).
	Allora definiamo la \emph{\textenglish{relative frequency density}} (chiamata anche densità \emph{stimata} di probabilità\footnote{Si noti che si stima la densità \emph{non} la probabilità, quindi i valori di \(\RFD\) possono essere maggiori di \(1\).}), indicata \(\RFD\), come
	\begin{equation}\label{eq:rfd}
		\RFD=\frac{F}{n\delta}
	\end{equation}
\end{defn}
In altre parole, la \(\RFD\) di un intervallo è il rapporto tra il numero di misure al suo interno, e il prodotto tra la sua lunghezza e la dimensione del \dataset.

Ora dimostreremo intuitivamente perché è possibile sovrapporre l'istogramma e la curva calcolata da \mintinline{R}|density|.
Consideriamo l'\cref{eq:rfd} e moltiplichiamo entrambi i membri per \(\delta\) così da ottenere
\begin{equation}\label{eq:rfd-rearr}
	\RFD\cdot\delta = \frac{F}{n}
\end{equation}
Osservando attentamente l'\cref{eq:rfd-rearr} ci accorgiamo che il membro sinistro calcola l'area (\(\mvar{altezza}\cdot\mvar{base}\)) del generico intervallo \(\symcal{B}\), mentre la quantità \(F\divslash n\) è la \emph{probabilità} che una misura di \(X\), scelta a caso, cada nell'intervallo \(\symcal{B}\).

In conclusione plottando la \(\RFD\), piuttosto che le frequenze, sull'asse verticale, otteniamo un istogramma la cui area rappresenta una probabilità.
Per verificare quanto detto l'utente è invitato a eseguire in una finestra di terminale R il seguente codice
\begin{minted}{R}
	set.seed(42)
	x <- rnorm(1000000)
	r <- hist(x, plot = FALSE)
	sum(r$density * as.double(diff(r$breaks))) == 1
\end{minted}
si noti che \mintinline{R}|as.double(diff(r$breaks))| calcola i \(\delta\) per ciascuna barra (intervallo) dell'istogramma, mentre \mintinline{R}|r$density| è il vettore contenente gli \(\RFD\) per ciascuna barra calcolato come \mintinline{R}|counts/(length(x) * as.double(diff(breaks)))| (vedi anche \mintinline{R}|ylim| nel \Cref{subsec:draw-plot}).