% !TeX encoding = UTF-8
% !TeX root = ../../../relazione.tex

\section{Tempo lagrangiano di scala}
\label{sec:lagrangian}

In questo esercizio l'obiettivo è determinare il tempo di scala lagrangiano della velocità del vento.
Per tanto cominciamo con il ribadire un concetto già visto nel \Cref{sec:pre-processing}: ovvero che ogni colonna dei nostri \dataset rappresenta una serie temporale, che non è nient'altro che un processo stocastico, la cui definizione, qui ripresa, è già stata incontrata nel corso.
\begin{defn}
	Si definisce \emph{processo stocastico} una collezione di variabili aleatorie \(\Set{X_t, t\in T}\) indicizzate su \(t\).
\end{defn}
Nelle serie temporali l'insieme degli indici (o parametri) \(T\subseteq\symbb{R}\) è un insieme di punti temporali.
Operativamente questo significa che ogni (impredicibile) valore che la velocità del vento assume in ogni minuto proviene da una variabile aleatoria il cui indice è proprio il minuto in questione.
Noi non conosciamo le distribuzioni di tali variabili, ma solo i valori osservati quindi abbiamo solo una possibile \emph{realizzazione} del processo stocastico.
In più osserviamo che la serie temporale considerata dovrebbe essere a parametro continuo, tuttavia a causa del campionamento, con frequenza di un minuto, fatto dalla centralina essa risulta a parametro discreto.

Inoltre per motivi che vederemo in seguito assumiamo che la turbolenza è stazionaria in piccole regioni del tempo e dello spazio (per i nostri scopi è sufficiente la stazionarietà in senso ampio), omogenea e isotropa.

\subsection{Funzione di autocorrelazione lagrangiana}
\label{subsec:lagrangian-acf-fun}

Le definizioni che abbiamo appena ripreso non bastano per renderci del tutto operativi, ne serve ancora qualcuna.
\begin{defn}
	Se \(\Set{X_t, t\in T}\) è un processo tale che \(\Var{X_{t}} < \infty\) per ogni \(t\in T\), allora la funzione di autocovarianza \(\gamma_{X}( \,{\cdot}\,,{\cdot}\,) \) di \(\Set{X_{t}}\) è definita come
	\begin{equation}\label{eq:acov}
		\gamma_{X}(t_1,t_2) = \Cov[\big]{ X_{t_1}, X_{t_2}} = \expect[\big]{\bigl( X_{t_{1}} - \expect[\big]{ X_{t_1}} \bigr)\bigl(X_{t_{2}} - \expect[\big]{ X_{t_2}}\bigr)}
		\qquad
		t_1,t_2\in T
	\end{equation}
\end{defn}
L'autocovarianza misura la dipendenza \emph{lineare} tra due punti della stessa serie temporale osservati a tempi differenti.
Tuttavia in statistica classica, si preferisce usare un \emph{indice} tra~\(-1\) e \(1\) per valutare la \emph{correlazione} tra caratteri statistici, perciò introduciamo la seguente funzione.
\begin{defn}
	La funzione di autocorrelazione è definita come
	\begin{equation}\label{eq:acor}
		\rho_{X}(t_1, t_2) = \frac{\Cov[\big]{X_{t_1}, X_{t_2}}}{\sigma_{t_{1}}\sigma_{t_{2}}} = \frac{\gamma_{X}(t_1,t_2)}{\sqrt{\gamma_{X}(t_1,t_1)\gamma_{X}(t_2,t_2)}}
	\end{equation}
\end{defn}
Tuttavia assumendo la turbolenza stazionaria in senso ampio si ha che la funzione di autocorrelazione dipende solo dal lag temporale \(\tau\) che intercorre tra i tempi \(t_1\) e \(t_2\).
Inoltre tutte le variabili aleatorie all'interno del processo hanno stessa media \(\mu\) e stessa varianza \(\sigma^{2}\).
Quindi possiamo semplificare le \cref{eq:acov,eq:acor} rispettivamente in
\begin{align*}
	\gamma_{X}(\tau) &= \Cov{ X_{t+h}, X_{t}} = \expect{(X_{t+\tau} - \mu)(X_{t} - \mu)}\\
	\rho_{X}(\tau) &= \frac{\gamma_{X}(t+\tau,t)}{\sqrt{\gamma_{X}(t+\tau,t+\tau)\gamma_{X}(t,t)}} = \frac{\gamma_{X}(\tau)}{\gamma_{X}(0)}
\end{align*}

Per lo scopo dell'esercizio non calcoleremo l'autocorrelazione, ma una sua \emph{stima}, questo perché non conosciamo le variabili aleatorie della serie temporale: infatti abbiamo a disposizione solo una realizzazione \(\lbrace x_t\rbrace\).
Per tanto è necessario definire i seguenti stimatori.
\begin{defn}
	La funzione di autocovarianza \emph{campionaria} di \(\Set{x_1, \dots, x_n}\) è definita come
	\begin{equation}\label{eq:sample-acov}
		\hat{\gamma}_{X}(\tau) = n^{-1}\sum_{t=1}^{n-\abs{\tau}}(x_{t+\abs{\tau}}-\bar{x})(x_{t}-\bar{x})
		\quad
		0\leq\tau<n
	\end{equation}
	e \(\hat{\gamma}_{X}(\tau) = \hat{\gamma}_{X}(-\tau)\), \(-n<\tau\leq0\), dove \(\bar{x}\) è la media campionaria \(\bar{x} = n^{-1}\sum_{j=1}^{n}x_j\).
\end{defn}
Lo stimatore in \labelcref{eq:sample-acov} ha valore atteso \(\expect{ \hat{\gamma}_{X}(\tau) } = (n-\abs{\tau})n^{-1}\gamma_{X}(\tau) \neq \gamma_{X}(\tau)\), ovvero quando \(\tau\neq0\) allora \(\hat{\gamma}_{X}(\tau)\) è non corretto.
Per tanto sembra più ragionevole considerare uno stimatore corretto ovvero dividendo per \(n-\abs{\tau}-1\), ma questo comporterebbe che la funzione non sarebbe \emph{definita non-negativa}.\footnote{Una funzione reale di variabile intera, \(\kappa\colon\symbb{Z}\to\symbb{R}\), è definita non-negativa se e solo se \begin{equation*}
		\sum_{i=1}^{n}\sum_{j=1}^{n}a_i\kappa(t_i-t_j) a_j \geq 0
\end{equation*}
per ogni intero positivo \(n\) e per ogni vettore \(\symbf{a} = (a_1, \dots, a_n)^{T}\in\symbb{R}^{n}\) e \(\symbf{t} = (t_1, \dots, t_n)^{T}\in\symbb{Z}^{n}\). Per la matrice \(\symbf{\Kappa} = \mathopen[\kappa(t_i-t_j)\mathclose]_{i,j=1}^{n}\), questo implica che \(\symbf{a}^{T}\symbf{\Kappa}\symbf{a}\geq0\) per ogni vettore \(\symbf{a}\).}
Quindi lo stimatore appena presentato ci permette di calcolare la matrice quadrata di covarianza \(\hat{\symbf{\Gamma}}_n = \mathopen[\hat{\gamma}_{X}(t_i-t_j)\mathclose]_{i,j=1}^{n}\) visto che ne rispettiamo tutte le proprietà: infatti la matrice è garantita essere definita non-negativa e simmetrica rispetto alla diagonale principale, perché la funzione in \labelcref{eq:sample-acov} è dispari.

\begin{defn}
	La funzione di autocorrelazione \emph{campionaria} è definita come
	\begin{equation}\label{eq:sample-acor}
		\hat{\rho}_{X}(\tau) = \frac{\hat{\gamma}_{X}(\tau)}{\hat{\gamma}_{X}(0)} = \frac{\sum_{t=1}^{n-\abs{\tau}}(x_{t+\abs{\tau}}-\bar{x})(x_{t}-\bar{x})}{\sum_{t=1}^{n}(x_{t+\abs{\tau}}-\bar{x})^{2}}
	\end{equation}
\end{defn}
La \labelcref{eq:sample-acor} è anche chiamata funzione di autocorrelazione lagrangiana ed è indicata con \(\hat{R}^L(\tau)\).
\begin{figure}[t]
	\centering
	\includegraphics*[trim={0 0 0 2cm},width=\linewidth]{acf.pdf}
	\caption{Funzione di autocorrelazione lagrangiana per il giorno 19~settembre 2018 con \(\text{\mintinline{R}{lag.max}} = 30\).}
	\label{fig:draw-acf}
\end{figure}
In \Cref{fig:draw-acf} è plottato \(\hat{R}^L(\tau)\) al variare di \(\tau\) per il giorno 19~settembre 2018, ottenuto eseguendo lo script \file{lagrangian.R}
\begin{minted}[linenos=false]{bash}
	./lagrangian.R settembre/2018-09-19.dat
\end{minted}
Il comando precedente poteva essere variato usando l'opzione \mintinline{bash}|-l|, che permette di specificare il massimo lag desiderato.
Se non specificato lo script usa il default, preso in prestito dalla funzione \mintinline{R}|acf| di R, \(10\log_{10}(N\divslash m)\) (\lref{min:loglagrangian} della \Cref{fig:acorf}), in cui \(N\) è il numero di osservazioni, fisso sempre a \(1440\) perché in un giorno ci sono \(1440\)~minuti, mentre \(m\) è il numero di serie temporali ed è fisso a \(1\), perché consideriamo solo le velocità del vento.
Infine si fa notare che lo script accetta solo singoli giorni e non diversi periodi temporali, come facevano gli script precedenti.

Quindi con l'opzione \mintinline{R}|-l| è possibile specificare un \emph{qualunque} \(\tau\), addirittura che non rispetti la condizione \(0\leq\tau<n\).
Tuttavia non ci sarebbe un fallimento nello script perché, così come fa la funzione \mintinline{R}|acf| di R, si è deciso di assegnare a \mintinline{R}|lag.max| il minimo tra l'input fornito dall'utente e il numero di osservazioni meno una (\lref{min:minchecklagrangian}).
Per quanto detto finora l'utente potrebbe fornire in input \mintinline{bash}|-l 0| o \mintinline{bash}|-l 1439|, ma i grafici prodotti sarebbero poco utili.
\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=52,lastline=77]{R}{lagrangian.R}
	\caption{Funzione \mintinline{R}{acorf}.}
	\label{fig:acorf}
\end{figure}
Nel primo caso la curva si riduce ad un punto dal quale scopriamo che l'indice di correlazione tra la serie temporale e se stessa è pari a \(1\), che non ci sorprende.
Nel secondo caso abbiamo che alla \lref{min:autocovariance} calcoleremmo, nell'ultima iterazione \(\hat{\gamma}_{X}(\text{\mintinline{R}|length(x) -|}\,1439 = 1)\), la cui stima, basata su una sola osservazione, sarebbe inaffidabile.

\subsection{Calcolo del tempo di scala lagrangiano}
\label{subsec:tl-from-rl}

Riguardo la \Cref{fig:draw-acf} non si è detto tutto: infatti in questo \namecref{subsec:tl-from-rl} spiegheremo come è stata tracciata la linea verticale tratteggiata etichettata con \(T_{\symrm{L}}\).
Innanzitutto va osservato che la curva di \(\hat{R}^L(\tau)\) mostra un decadimento esponenziale, che è caratteristica di un processo markoviano la cui funzione di autocorrelazione temporale può essere scritta come \(R(\tau) = \exp(-\tau\divslash T_{\symrm{L}})\).
Inoltre per ipotesi di turbolenza omogenea possiamo scrivere \(\hat{R}^L(\tau)\approx\exp(-\tau\divslash T_{\symrm{L}})\), ovvero approssimiamo la funzione di autocorrelazione lagrangiana con quella markoviana, sebbene quest'ultima cala troppo rapidamente man mano che i lag aumentano, come si osserva dalla curva tratteggiata-punteggiata in \Cref{fig:draw-acf}.

\begin{figure}[t]
	\centering
	\pathinputminted[escapeinside=££,firstline=87,lastline=90]{R}{lagrangian.R}
	\caption{Estratto della funzione \mintinline{R}{main} dello script \file{lagrangian.R}.}
	\label{fig:main-lagrangian}
\end{figure}
Per completare l'esercizio non ci rimane che ricavare l'\emph{unico parametro} \(T_{\symrm{L}}\), per far ciò sfrutteremo la regressione esponenziale.
Essa consiste nel determinare, tra tutte le curve di equazione \(y=c\eu^{ax}\) quella che massimizza il grado di accostamento a \((x_1,y_1)\), \((x_2,y_2)\), \dots, \((x_n,y_n)\).
Tuttavia nel nostro caso possiamo porre \(c=1\) e restringerci alla sola classe \(y=\eu^{ax}\), detto ciò consideriamo i logaritmi naturali di entrambi i membri \(\ln y = \ln\eu^{ax}\implies\ln y = {ax}\) e poniamo \(z=\ln y\).
Quindi l'equazione \(y=\eu^{ax}\) diviene così \(z=ax\) in tal modo il problema è ricondotto a quello dell'interpolazione lineare.

In \Cref{fig:main-lagrangian} è riportato il codice per eseguire la regressione in R\@.
Alla \lref{min:retacorf} la lista, con le componenti \mintinline{R}|x| e \mintinline{R}|y|, ritornata dalla funzione \mintinline{R}|acorf| viene passata come secondo argomento a \mintinline{R}|lm| (\lref{min:lm}), mentre la \emph{formula} \mintinline{R}|log(y) ~ 0 + x| viene passata come primo argomento.
Si noti che la sintassi usata per la formula non appartiene alla matematica, ma al linguaggio R; inoltre l'uso di \mintinline{R}|0| indica alla funzione che non vi è intercetta, ma solo coefficiente angolare.
Successivamente dalla posizione \mintinline{R}|1| del vettore \mintinline{R}|coefficients| (di lunghezza \(1\)) componente dell'oggetto \mintinline{R}|fm| di classe \mintinline{R}|lm| viene estratto \(-1\divslash T_{\symrm{L}}\) per tanto è necessario calcolarne il reciproco e l'opposto come mostrato alla \lref{min:tL}.
Infine l'oggetto \mintinline{R}|tL| può essere aggiunto alla \Cref{fig:draw-acf} con \mintinline{R}|abline(v = tL, lty = "longdash")| come l'utente stesso può verificare ispezionando la funzione \mintinline{R}|draw_plot|.

Si conclude sottolineando che la funzione \mintinline{R}|acorf| è stata confrontata con la più generica \mintinline{R}|acf|, come mostra il commento di \lref{min:acf}, osservando che vengono forniti gli stessi risultati: questo perché lo script, come altri, prende molto spunto dal linguaggio R\@.
