#! /usr/bin/env Rscript

library("docopt")
source("utils/read_dataset.R")
source("utils/show_plot.R")
source("utils/save_as_pdf.R")

"
Calcolo del giorno tipo per la temperatura.

Usage:
  typical-day.R [-s <value>] [(--pdf <file> [(-w <value> -H <value>)] [-f] [-q])] [-t <value>] <day> <day>...
  typical-day.R (-h | --help)
  typical-day.R --version

Options:
  -h --help                       Show this screen.
  --version                       Show version.
  -t <value>, --title <value>     Set title for plot.
  -s <value>, --step <value>      Set step (in minutes) for break in plot.
                                  Minimum value 1, maximum value 1339.
                                  Default 15.
  --pdf <file>                    Save plots as PDF.
  -H <value>, --height <value>    Height (in mm) for PDF.
                                  Available with --pdf.
  -w <value>, --width <value>     Width (in mm) for PDF.
                                  Available with --pdf.
  -f, --force                     Force overwriting to save PDF.
  -q, --quiet                     Do not output any plots.
                                  Available with --pdf.

Examples:
  $ ./typical-day.R ../../dati-20211125/settembre/2018-09-0{1..8}.dat -s 30 -t 'Giorno tipo di una settima di settembre 2018' --pdf /tmp/bar.pdf -H 300 -w 300
  $ ./typical-day.R ../../dati-20211125/settembre/*.dat -s 60 -t 'Giorno tipo di settembre 2018'

Copyright (c) 2022
" ->> doc

draw_ts <- function(data, title, step, minutes) {
  par(mar = c(7, 4, 4, 2) + .1, mgp=c(1.6,.6,0))
  plot(data, type = "n", xaxt = "n", xlab = "", ylab = "Average temperature", main = title)
  indexes <- seq(1, 60 * 24, step)
  axis(1, at = indexes, tck = 1, lty = 1, col = gray(.9), labels = FALSE)
  labels <- minutes[indexes]
  text(indexes, par("usr")[3] - .25, srt = 90, adj = 1, labels = labels, xpd = TRUE)
  mtext(1, text = "Minutes", line = 4.5)
  grid(NA, NULL, lty = 1, col = gray(.9))
  lines(data)
  #Add box around plot
  box()
}

main <- function() {
  arguments <- docopt(doc, version = "giorno-tipo 2.1\n")

  if (is.null(arguments$step)) {
    step <- 15
  } else {
    step <- suppressWarnings(as.integer(arguments$step))
    if (length(step) != 1 || is.na(step) || step < 1 || step > 1439) {
      err_msg <- paste(
        "`step` must be an integer belonging to [1,1439].",
        paste0("You've supplied ", arguments$step, "."),
        sep = "\n"
      )
      stop(err_msg, call. = FALSE)
    }
  }

  title <- arguments$title

  days <- lapply(arguments$day, read_dataset)£\Vlabel{min:typicaldaylapply}£

  xy <- list(
    x = 1:(60 * 24),
    y = rowMeans(sapply(lapply(days, "[[", "df"), "[[", "TemperaturaAria"))£\Vlabel{min:typicaldaymean}£
  )

  if (is.null(arguments$height)) {
    arguments$width <- 300
    arguments$height <- 150
  } else if (as.numeric(arguments$height) >= as.numeric(arguments$width)) {
    warning(
      "The ideal shape for plotting time series, in most instances, is when the time axis is much wider than the value axis.",
      call. = FALSE, immediate. = TRUE
    )
  }

  if (! arguments$quiet) {
    show_plot(draw_ts, xy, title, step, row.names(days[[1]]$df))
    wait()
  }

  if (! is.null(arguments$pdf)) {
    save_as_pdf(
      file = arguments$pdf,
      fun = draw_ts, xy, title, step, row.names(days[[1]]$df),
      width = arguments$width,
      height = arguments$height,
      force = arguments$force
    )
  }
}

main()
q(status = 0)
