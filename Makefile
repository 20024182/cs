# Executables
latexmk = latexmk

# Main file name
MAIN = FERRON_relazione
MASTER = relazione
MASTER_TEX = $(MASTER).tex
WORKING_DIR = src/

TARGETS = readable

all: $(TARGETS) figures

$(TARGETS):
	cd $(WORKING_DIR) && \
	mkdir -vp ../out && \
	rm -vf $(MASTER).pdf && \
	$(latexmk) --halt-on-error --file-line-error -lualatex -usepretex='\providecommand{\outputformat}{$@}' $(MASTER_TEX) && \
	cp -v $(MASTER).pdf ../out/$(MAIN)-$@.pdf

check: all
	curl -sL 'https://dev.verapdf-rest.duallab.com/api/validate/3b/' \
	  -H 'Accept: application/json' \
	  -H "X-File-Size: $(shell stat --printf='%s' "./out/$(MAIN)-readable.pdf")" \
	  -F file="@./out/$(MAIN)-readable.pdf" \
	  -F sha1Hex="$(shell sha1sum "./out/$(MAIN)-readable.pdf" | cut -d ' ' -f 1)" \
	  -H 'DNT: 1' \
	  -H 'Cache-Control: no-cache' \
	  -H 'Sec-GPC: 1' | jq -r -e '.report.jobs[].validationResult[].compliant'
	# TODO:
	#  - check if all figures have fonts embedded

figures:
	find ./src/contents/ -path '*/assets/img/*' ! -name '.gitkeep' ! -name '*-eps-converted-to.pdf' ! -iname '*.tikz' | zip -@ -j ./out/figures.zip

.PHONY: check
